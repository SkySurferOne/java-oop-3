package edu.agh.javaoop;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;

/**
 * Created by Damian on 2016-05-12.
 */
public class HiringStrategyTest {
    private Employee emp1, emp2, emp3, emp4, emp5;

    @Before
    public void setUp() throws Exception {
        emp1 = new Employee("Mark", 3000);
        emp2 = new Employee("Kean", 3000);
        emp3 = new Employee("Rob", 3000);
        emp4 = new Employee("Mark", 1000);
        emp5 = new Employee("Kean", 9000);
    }

    @Test
    public void testBudgetHiringStrategyExceedingTheLimit() {
        // given
        Manager manager = new Manager("Daniel", 13000, new HiringStrategyBasedOnBudget(10000));

        // when
        manager.hire(emp1);
        manager.hire(emp2);
        manager.hire(emp3);
        manager.hire(emp4);

        // then
        Assert.assertFalse(manager.canHire(emp5));
    }

    @Test
    public void testPlaceLimitHiringStrategyExceedingTheLimit() {
        // given
        Manager manager = new Manager("Adam", 9000, new HiringStrategyBasedOnPlaceLimit(4));

        // when
        manager.hire(emp1);
        manager.hire(emp2);
        manager.hire(emp3);
        manager.hire(emp4);

        // then
        Assert.assertFalse(manager.canHire(emp5));
    }

}
