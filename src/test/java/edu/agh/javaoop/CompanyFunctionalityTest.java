package edu.agh.javaoop;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Damian on 2016-05-12.
 */
public class CompanyFunctionalityTest {
    private Company company = Company.getInstance();

    @Before
    public void setUp() {
        company.clear();
    }

    @Test
    public void testCompanyToString() {

    }

    @Test
    public void testGetOverallSalaries() {
        // given
            // CEOs
        CEO ceo = new CEO("Marcin", 20000, new HiringStrategyBasedOnBudget(100000));

            // Managers
        Manager manBudget = new Manager("Daniel", 13000, new HiringStrategyBasedOnBudget(100000));
        Manager manPlaceLimit = new Manager("Maria", 16000, new HiringStrategyBasedOnPlaceLimit(3));

            //  Employees
        Employee emp1 = new Employee("Jakub", 7000);
        Employee emp2 = new Employee("Kean", 9000);
        Employee emp3 = new Employee("Rob", 10000);

        // when
        company.hireCEO(ceo);
        ceo.hire(manBudget);
        ceo.hire(manPlaceLimit);
        manBudget.hire(emp1);
        manPlaceLimit.hire(emp2);
        manPlaceLimit.hire(emp3);

        // then
        Assert.assertEquals(20000 + 13000 + 16000 + 7000 + 9000 + 10000, company.getOverallSalaries(), 0);
    }

    @Test
    public void testLowestSalary() {
        // given
            // CEOs
        CEO ceo = new CEO("Marcin", 20000, new HiringStrategyBasedOnBudget(100000));

            // Managers
        Manager manBudget = new Manager("Daniel", 13000, new HiringStrategyBasedOnBudget(100000));

            //  Employees
        Employee emp1 = new Employee("Jakub", 1000);
        Employee emp2 = new Employee("Kean", 9000);
        Employee emp3 = new Employee("Rob", 10000);

        // when
        company.hireCEO(ceo);
        ceo.hire(manBudget);
        manBudget.hire(emp1);
        manBudget.hire(emp2);
        manBudget.hire(emp3);

        // then
        Assert.assertEquals(1000, company.getGuyWithLowestSalary().getSalary(), 0);
    }

    @Test
    public void testGetUnsatisfiedEmployees() {
        // given
            // CEOs
        CEO ceo = new CEO("Marcin", 20000, new HiringStrategyBasedOnBudget(100000));

            // Managers
        Manager manBudget = new Manager("Daniel", 13000, new HiringStrategyBasedOnBudget(100000));

            //  Employees
        Employee emp1 = new Employee("Jakub", 900);
            emp1.setSatisfactionStrategy(new SalarySatisfactionStrategy(1000));
        Employee emp2 = new Employee("Kean", 9000);
            emp2.setSatisfactionStrategy(new SalarySatisfactionStrategy(1000));
        Employee emp3 = new Employee("Rob", 10000);
            emp3.setSatisfactionStrategy(new SalarySatisfactionStrategy(1000));

        // when
        company.hireCEO(ceo);
        ceo.hire(manBudget);
        manBudget.hire(emp1);
        manBudget.hire(emp2);
        manBudget.hire(emp3);

        // then
        Assert.assertEquals(2, company.getEmployeesWho(emp -> emp.isSatisfied()).size(), 0);
        Assert.assertEquals(1, company.getEmployeesWho(emp -> !emp.isSatisfied()).size(), 0);
    }

}
