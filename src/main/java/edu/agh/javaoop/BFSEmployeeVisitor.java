package edu.agh.javaoop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Damian on 2016-05-12.
 */
public class BFSEmployeeVisitor implements EmployeeVisitor {
    private final List<Employee> resultList = new ArrayList<Employee>();

    public void visit(Employee employee) {}

    public void visit(Manager manager) {
        if(resultList.isEmpty())
            resultList.add(manager);

        for (Employee person: manager.getListOfEmployees()) {
            resultList.add(person);
        }

        for (Employee person: manager.getListOfEmployees()) {
            person.accept(this);
        }

    }

    public void visit(CEO ceo) {
        if(resultList.isEmpty())
            resultList.add(ceo);

        for (Employee person: ceo.getListOfEmployees()) {
            resultList.add(person);
        }

        for (Employee person: ceo.getListOfEmployees()) {
            person.accept(this);
        }
    }

    public void visit(List<CEO> listOfCEOs) {
        for (Employee person: listOfCEOs) {
            resultList.add(person);
        }

        for (CEO person : listOfCEOs) {
            person.accept(this);
        }
    }

    public List<Employee> getResultList() {
        return resultList;
    }
}
