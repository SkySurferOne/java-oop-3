package edu.agh.javaoop;

import java.util.List;

/**
 * Created by Damian on 2016-05-04.
 */
public class HiringStrategyBasedOnBudget extends HiringStrategy {
    private double budget;

    public HiringStrategyBasedOnBudget(double budget) {
        this.budget = budget;
    }

    public double getUsedBudged() {
        double tmp = 0;
        for (Employee person : listOfEmployees) {
            tmp += person.getSalary();
        }
        return tmp;
    }

    public boolean canHire(Employee person) {
        return (budget - getUsedBudged()) >= person.getSalary();
    }

    public void hire(Employee person) {
        if(canHire(person)){
            listOfEmployees.add(person);
        } else {
            throw new IllegalArgumentException("You cannot hire this person because your budget is too small."
                    + "Yours current budget: " + budget + "Salary of the person you want to hire: " + person.getSalary());
        }
    }
}
