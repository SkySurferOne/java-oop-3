package edu.agh.javaoop;

import java.util.List;

/**
 * Created by Damian on 2016-05-12.
 */
public interface EmployeeVisitor {
    void visit(Employee person);
    void visit(Manager person);
    void visit(CEO person);
    void visit(List<CEO> listOfCEOs);
}
