package edu.agh.javaoop;

import java.util.List;

/**
 * Created by Damian on 2016-05-12.
 */
public class Manager extends Employee {
    protected HiringStrategy hiringStrategy = null;

    public Manager(String name, int salary, HiringStrategy hiringStrategy) {
        super(name, salary);
        this.hiringStrategy = hiringStrategy;
    }

    public List<Employee> getListOfEmployees() {
        return hiringStrategy.getListOfEmployees();
    }

    public boolean canHire(Employee person) {
        return hiringStrategy.canHire(person);
    }

    public void hire(Employee person) {
        hiringStrategy.hire(person);
    }
}
