package edu.agh.javaoop;

/**
 * Created by Damian on 2016-05-12.
 */
public class SalarySatisfactionStrategy implements SatisfactionStrategy {
    private double satisfyingSalary = 10000;

    public SalarySatisfactionStrategy(double satisfyingSalary) {
        this.satisfyingSalary = satisfyingSalary;
    }

    public void setSatisfaingSalary(int salary) {
        if(salary < 0.0)
            throw new IllegalArgumentException("Satisfying salary must be greater than 0");
        this.satisfyingSalary = salary;
    }

    public double getSatisfyingSalary() { return satisfyingSalary; }

    @Override
    public boolean isSatisfied(Employee employee) {
        return (employee.getSalary() > satisfyingSalary);
    }
}
