package edu.agh.javaoop;

/**
 * Created by Damian on 2016-05-04.
 */
public class Employee implements Visitable {
    final protected String name;
    protected double salary;
    protected SatisfactionStrategy satisfactionStrategy = new SalarySatisfactionStrategy(10000);

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public double getSalary() { return salary; }

    public void setSalary(int salary) {
        if(salary < 0.0)
            throw new IllegalArgumentException("Salary must be greater than 0");
        this.salary = salary;
    }

    public void setSatisfactionStrategy(SatisfactionStrategy strategy) {
        satisfactionStrategy = strategy;
    }

    public boolean isSatisfied() { return satisfactionStrategy.isSatisfied(this); }

    @Override
    public void accept(EmployeeVisitor visitor) {
        visitor.visit(this);
    }
}
