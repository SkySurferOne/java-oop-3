package edu.agh.javaoop;

/**
 * Created by Damian on 2016-05-04.
 */
public class HiringStrategyBasedOnPlaceLimit extends HiringStrategy {
    private int placeLimit;

    public HiringStrategyBasedOnPlaceLimit(int placeLimit) {
        this.placeLimit = placeLimit;
    }

    public int getUsedPlaces() {
        return listOfEmployees.size();
    }

    boolean canHire(Employee person) {
        return (getUsedPlaces() + 1 <= placeLimit);
    }

    void hire(Employee person) {
        if(canHire(person)){
            listOfEmployees.add(person);
        } else {
            throw new IllegalArgumentException("You cannot hire this person because you used all places."
                    + "Yours place limit is: " + placeLimit);
        }
    }
}
