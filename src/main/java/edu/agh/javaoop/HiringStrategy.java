package edu.agh.javaoop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Damian on 2016-05-04.
 */
public abstract class HiringStrategy {
    final protected List<Employee> listOfEmployees = new ArrayList<Employee>();

    public List<Employee> getListOfEmployees() {
        return listOfEmployees;
    }
    abstract boolean canHire(Employee person);
    abstract void hire(Employee person);
}
