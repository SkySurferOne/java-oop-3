package edu.agh.javaoop;

import java.util.List;

/**
 * Created by Damian on 2016-05-12.
 */
public class SalaryCalculateEmployeeVisitor implements EmployeeVisitor {
    private double summary = 0;

    @Override
    public void visit(Employee employee) {
        summary += employee.getSalary();
    }

    @Override
    public void visit(Manager manager) {
        summary += manager.getSalary();
        for(Employee person : manager.getListOfEmployees()) {
            person.accept(this);
        }
    }

    @Override
    public void visit(CEO ceo) {
        summary += ceo.getSalary();
        for(Employee person : ceo.getListOfEmployees()) {
            person.accept(this);
        }
    }

    @Override
    public void visit(List<CEO> listOfCEOs) {
        for (CEO person : listOfCEOs) {
            person.accept(this);
        }
    }

    public double getSummary() {
        return summary;
    }
}
