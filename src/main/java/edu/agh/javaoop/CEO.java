package edu.agh.javaoop;

/**
 * Created by Damian on 2016-05-12.
 */
public class CEO extends Manager {
    public CEO(String name, int salary, HiringStrategy hiringStrategy) {
        super(name, salary, hiringStrategy);
    }
}
