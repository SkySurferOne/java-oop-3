package edu.agh.javaoop;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by Damian on 2016-05-12.
 */
public class Company {
    private List<CEO> listOfCEOs = new ArrayList<CEO>();

    private static Company ourInstance = new Company();

    public static Company getInstance() {
        return ourInstance;
    }

    private Company() {}

    public void hireCEO(CEO person) {
        listOfCEOs.add(person);
    }

    public double getOverallSalaries() {
        SalaryCalculateEmployeeVisitor salaryCalcVisitor = new SalaryCalculateEmployeeVisitor();

        salaryCalcVisitor.visit(listOfCEOs);

       return salaryCalcVisitor.getSummary();

    }

    public Employee getGuyWithLowestSalary() {
        Employee poorestGuy = listOfCEOs.get(0);

        for (Employee person : getListOfEmployeesBFS()) {
            if (person.getSalary() < poorestGuy.getSalary())
                poorestGuy = person;
        }

        return poorestGuy;
    }

    public List<Employee> getListOfEmployeesBFS(){
        BFSEmployeeVisitor BFSVisitor = new BFSEmployeeVisitor();

        BFSVisitor.visit(listOfCEOs);

        return BFSVisitor.getResultList();
    }

     public List<Employee> getEmployeesWho(Predicate<Employee> p){
         List<Employee> resultList = new ArrayList<Employee>();

         for (Employee person: getListOfEmployeesBFS()) {
             if(p.test(person))
                 resultList.add(person);
         }

         return resultList;
     }

    public void clear() {
        listOfCEOs.clear();
    }

    // TODO: 2016-05-12
    // make it iterable in BFS style

    @Override
    public String toString() {
        return "";
    }
}
