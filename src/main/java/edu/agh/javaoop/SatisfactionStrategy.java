package edu.agh.javaoop;

/**
 * Created by Damian on 2016-05-12.
 */
public interface SatisfactionStrategy {
    boolean isSatisfied(Employee employee);
}
